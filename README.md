# README #

This git repository will be available for submission of your iPython notebooks for ENVE T480/T580.

### Motivation ###

* Using version control software is a best practice for modelers.
* Submitting code here will make it easier to evaluate it.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact